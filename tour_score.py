import pandas as pd
from sklearn.preprocessing import minmax_scale


def prepare_tour_score(tour_score_df: pd.DataFrame) -> pd.DataFrame:
    """prepare tour_scores

    Args:
        tour_score_df (pd.DataFrame):

    Returns:
        pd.DataFrame:

    """
    tour_score_df['tour_score_scale'] = tour_score_df['tour_score'].\
        transform(lambda x: minmax_scale(x.astype(float), feature_range=(1, 2)))
    return tour_score_df[['tour_id', 'tour_score_scale']]
