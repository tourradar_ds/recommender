import numpy as np
import pandas as pd
from sklearn.preprocessing import minmax_scale


def wilson_confidence(trial,
                      success,
                      z=1.96) -> pd.DataFrame:
    """wilson confidence implementations

    Args:
        trial (int):
        success (int)
        z=1.96

    Returns:
        pd.DataFrame: with relative error in click/impressions

    """
    n = trial
    ns = success
    nf = trial-success
    z_sq = z**2
    conf = (z/(nf+z_sq))*np.sqrt((z_sq/4)+(ns*nf/n))
    conv = (ns+(z_sq/4))/(n+z_sq)
    return conf/conv


def confidence(trial: list,
               success: list) -> pd.DataFrame:
    """confidence implementations (x-sqrt(x))/(y+sqrt(y))

    Args:
        trial (int):
        success (int)

    Returns:
        list

    """
    n = trial
    ns = success
    conf = (ns-ns**0.5)/(n+n**0.5)
    return conf


def adjust_ratios(ctr_df: pd.DataFrame,
                  revenue_df: pd.DataFrame) -> pd.DataFrame:
    """apply wilson confidence for clicks and impressions and scale

    Args:
        data (pd.DataFrame): clicks and impressions dataframe

    Returns:
        pd.DataFrame

    """
    rev_stats = revenue_df[
        pd.to_datetime(revenue_df['booking_date']) >= '2019-11-01']\
        .groupby(["tour_id"], observed=True, sort=False, as_index=False)\
        .agg({'net_revenue': 'sum', 'nbooking': 'sum'})
    ctr_rev = ctr_df.merge(rev_stats,
                           on='tour_id',
                           how='left').fillna(0)
    ctr_rev['conf_ctr'] = confidence(trial=ctr_rev['impressions'], success=ctr_rev['clicks'])
    ctr_rev['conf_booking'] = confidence(trial=ctr_rev['impressions'], success=ctr_rev['nbooking'])
    ctr_rev['conf_revenue'] = confidence(trial=ctr_rev['impressions'], success=ctr_rev['net_revenue'])
    ctr_rev['scaled_ctr_rate'] = ctr_rev['conf_ctr'].\
        transform(lambda x: minmax_scale(x.astype(float), feature_range=(1, 2)))
    ctr_rev['scaled_bookings_rate'] = ctr_rev['conf_booking'].\
        transform(lambda x: minmax_scale(x.astype(float), feature_range=(1, 2)))
    ctr_rev['scaled_rev_rate'] = ctr_rev['conf_revenue'].\
        transform(lambda x: minmax_scale(x.astype(float), feature_range=(1, 2)))
    return ctr_rev[['tour_id', 'scaled_ctr_rate', 'scaled_bookings_rate', 'scaled_rev_rate']]
