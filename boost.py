import pandas as pd


def get_tour_boost(pareto_boost_df: pd.DataFrame,
                   ctr_df: pd.DataFrame,
                   tour_score_df: pd.DataFrame,
                   ) -> pd.DataFrame:
    """summarize assisted conversions file.

    Args:
        pareto_boost_df (pd.DataFrame):
        ctr_df (pd.DataFrame):
        tour_score_df (pd.DataFrame):
    Returns:
        pd.DataFrame:

    """
    side_info = ctr_df.merge(pareto_boost_df, on='tour_id', how='left')\
                      .fillna(1)\
                      .merge(tour_score_df, on='tour_id', how='left')\
                      .fillna(1)

    side_info['boost'] = side_info['scaled_ctr_rate']*side_info['scaled_bookings_rate']*side_info['scaled_rev_rate']*side_info['pareto_boost']*side_info['tour_score_scale']
    side_info['boost_scaled'] = (side_info['boost']-1)/(19.2-1)+1
    return side_info[['tour_id', 'boost_scaled']]
