import pandas as pd
from scale import scale_similarity


def prepare_dimsum_similarity(dimsum_similarity_df: pd.DataFrame) -> pd.DataFrame:
    """prepare dimsum similarity data

    Args:
        dimsum_similarity_df (pd.Dataframe):
    Returns:
        pd.DataFrame:
    """
    scaled_df = scale_similarity(df=dimsum_similarity_df)
    scaled_df['tour_id'] = scaled_df['tour_id'].astype(int)
    scaled_df['similar_tour_id'] = scaled_df['similar_tour_id'].astype(int)
    return scaled_df[['tour_id', 'similar_tour_id', 'dimsum_similarity']]


def combine_similarities(graph_similarity_df: pd.DataFrame,
                         dimsum_similarity_df: pd.DataFrame) -> pd.DataFrame:
    """combine dimsum similarity and graph similarities

    Args:
        graph_similarity_df (pd.Dataframe):
        dimsum_similarity_df (pd.Dataframe):
    Returns:
        pd.DataFrame:
    """
    sim_df = graph_similarity_df.merge(
                                      dimsum_similarity_df,
                                      on=['tour_id', 'similar_tour_id'],
                                      how='left').fillna(1)
    sim_df['similarity'] = sim_df['graph_similarity'] * sim_df['dimsum_similarity']
    sim_df = sim_df[['tour_id', 'similar_tour_id', 'similarity']]
    dimsum_df = dimsum_similarity_df.merge(
                                  graph_similarity_df,
                                  on=['tour_id', 'similar_tour_id'],
                                  how='left').fillna(-1)
    dimsum_df = dimsum_df[dimsum_df['graph_similarity'] == -1]
    dimsum_df = dimsum_df[['tour_id', 'similar_tour_id', 'dimsum_similarity']]\
        .rename(columns={'dimsum_similarity': 'similarity'})
    return pd.concat([sim_df, dimsum_df], axis=0)