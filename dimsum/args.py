import argparse

__VERSION__ = "1.0.0"


def make_parser_item_item():
    """Make command line parser."""
    parser = argparse.ArgumentParser(
        description=f"%(prog)s (version {__VERSION__}). RECOMMENDER.",
        epilog="~~~ TourRadar [DS] ~~~",
    )
    parser.add_argument(
        "--version", action="version", version=f"%(prog)s (version {__VERSION__})"
    )
    parser.add_argument(
        "-i",
        "--input",
        help=f"item user interaction file",
        metavar="item_user_interaction",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-srp",
        "--serp",
        help=f"serp definitions",
        metavar="serp_definition",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-o",
        "--output",
        help=f"Output path",
        metavar="OUTPUT",
        type=str,
        required=True
    )
    parser.add_argument(
        "--similarity_tr",
        help="similarity threshold",
        type=float,
        default=0.05
    )
    return parser.parse_args()
