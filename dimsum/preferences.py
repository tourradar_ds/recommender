from pyspark.sql.functions import udf
from pyspark.sql.types import FloatType
from pyspark.sql.functions import col


def get_action_importances(click_value: int,
                           book_now_value: int,
                           purchase_value: int):
    """assign values for different type of actions

      Args:
        click_value:
        book_now_value:
        purchase_value:

    Returns:
        Dict
    """
    return {'click': click_value, 'book_now': book_now_value, 'purchase': purchase_value}

# thomas comment for bookers lets have bit higher and for non bookers same


def aggregate_user_preferences(data,
                               column: str,
                               action_importances: dict):
    """aggregate user preferences

    Args:
        data(SparkDataFrame): SparkDataFrame
        column (str): column name to aggregate
        action_importances(list): list of preferences

    Returns:
        SparkDataFrame
    """
    function = udf(lambda x: action_importances['click'] if x == '1' else action_importances['book_now'] if x== '2' else 6)
    df = data.withColumn('new_type', function(column).cast(FloatType()))
    df = df.withColumn('new_value', col('new_type')*col('interest'))
    df_agg = df.groupBy('user_id', 'tour_id')\
               .sum("new_value")\
               .withColumnRenamed("sum(new_value)", "type")
    return df_agg
