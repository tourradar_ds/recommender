import operator
import logging

from pyspark.sql import SparkSession
from pyspark.sql.functions import explode, col
spark = SparkSession.builder.appName('Recommender').getOrCreate()
spark.sparkContext.setLogLevel("ERROR")
spark.sparkContext.addFile("/home/tina/Git/data-pipelines/etl/jobs/ds/recommender_api/dimsum/DataframeToRDD.py")
spark.sparkContext.addFile("/home/tina/Git/data-pipelines/etl/jobs/ds/recommender_api/dimsum/dimsum.py")
spark.sparkContext.addFile("/home/tina/Git/data-pipelines/etl/jobs/ds/recommender_api/dimsum/proccess.py")
spark.sparkContext.addFile("/home/tina/Git/data-pipelines/etl/jobs/ds/recommender_api/dimsum/read_write.py")
spark.sparkContext.addFile("/home/tina/Git/data-pipelines/etl/jobs/ds/recommender_api/dimsum/serps.py")

from args import make_parser_item_item
from DataframeToRDD import convert_to_RDD_UI
from dimsum import tour_pq, _run_DIMSUM, get_similarity_data
from proccess import preprocess
from serps import get_d_serp_data, get_serps_url
from read_write import create_empty_dataframe, write_spark_csv_one_file

sc = spark.sparkContext


def setup_logger():
    level = logging.INFO
    c_format = logging.Formatter(
        "%(asctime)s - %(levelname)s - %(message)s")
    c_handler = logging.StreamHandler(sys.stdout)
    c_handler.setLevel(level)
    c_handler.setFormatter(c_format)
    logging.getLogger('').addHandler(c_handler)
    logging.getLogger('').setLevel(level)


logger = logging.getLogger(__name__)


def dimsum(data, sim_threshold: float):
    """run dimsum algorithm

    Args:
        data (SparkDataFrame): proccessed data of user item interactions

    Returns:
        SparkDataFrame: dataframe item item similarity

    """
    data_rdd = convert_to_RDD_UI(data=data)
    logger.info(
        "input data summary: %s",
        data_rdd.take(5)
    )
    get_norms = tour_pq(sc=sc, data=data_rdd, threshold=sim_threshold)
    get_similarity = (data_rdd.flatMap(lambda x: _run_DIMSUM(x[1], get_norms))
                              .reduceByKey(operator.add)
                              .filter(lambda x: x[1] > sim_threshold))
    similarity_df = get_similarity_data(data=get_similarity)
    similarity_df = similarity_df.withColumn("similar_tours", explode(similarity_df.similar_tour))
    results = similarity_df.select('tour_id', 'similar_tours.*')
    results = results.select(col("tour_id"),
                             col("_1").alias("similar_tour_id"),
                             col("_2").alias("similarity"))
    return results


def run_similarity_global(input: str, sim_threshold: float):
    """run dimsum algorithm

    Args:
        input (str): bigquery data user item interaction

    Returns:
        SparkDataFrame: dataframe item item similarity

    """
    # get_data = preprocess_data(sc=sc, input=input)
    logger.info(
        "input data summary"
    )
    get_data = preprocess(sc_files=input)
    logger.info(
        "input data summary:  %s",
        get_data.show(5))
    results = dimsum(data=get_data, sim_threshold=sim_threshold)
    return results


def run_similarity_per_serp(input: str, input_serp: str, sim_threshold: float):
    """run dimsum algorithm

    Args:
        input (str): bigquery data user item interaction
        input_serp (str) : serp data

    Returns:
        SparkDataFrame: dataframe item item similarity

    """
    # -- get_data = preprocess_data(sc=sc, input=input)
    logger.info(
        "input data summary"
    )
    get_interaction_data = preprocess(sc_files=input)
    # serps
    serp_data = get_d_serp_data(serp_input=input_serp)
    serps = get_serps_url(data=serp_data)
    get_data_serp = get_interaction_data.join(serp_data,
                                              get_interaction_data.tour_id == serp_data.tour_id1, how='left')
    # create empty dataframe
    results = create_empty_dataframe().cache()
    for i in serps:
        df_serp = dimsum(data=get_data_serp.filter(get_data_serp["serp_url"].isin([i])),
                         sim_threshold=sim_threshold)
        results = results.union(df_serp)
    return results


def train_dimsum_similarity(similarity_input: str,
                            serp_input: str,
                            output_path: str,
                            sim_threshold: float):
    """run dimsum algorithm global and per serp

    Args:
        similarity_input (str): bigquery data user item interaction
        serp_input (str) : serp data

    Returns:
        pd.DataFrame: dataframe item item similarity

    """
    sim_global = run_similarity_global(input=similarity_input,
                                       sim_threshold=sim_threshold)
    sim_per_serp = run_similarity_per_serp(input=similarity_input,
                                           input_serp=serp_input,
                                           sim_threshold=sim_threshold)
    similarity_data_pd = sim_global.union(sim_per_serp)
    logger.info(
        "count pairwise similarities: %s",
        similarity_data_pd.count()
    )
    write_spark_csv_one_file(data=similarity_data_pd, output_name=output_path)


def main():
    """Make cli, run trainer.
    example command line:
    """
    setup_logger()

    parsed = make_parser_item_item()
    logger = logging.getLogger(__name__)
    logger.info("Options: %s", parsed)
    train_dimsum_similarity(
        similarity_input=parsed.input,
        sim_threshold=parsed.similarity_tr,
        serp_input=parsed.serp,
        output_path=parsed.output,
    )


if __name__ == "__main__":
    main()
