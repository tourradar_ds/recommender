from pyspark.sql.functions import count, lit, desc, col
from pyspark.sql.types import *
from read_write import read_spark_csv


def get_serps_url(data):
    """pick serps which has above threshold tours

    Args:
        data (SparkDataFrame)
    Returns:
        list
    """
    serps = data.groupBy('serp_url')\
                .agg(count(lit(1)).alias("number_of_tours"))\
                .orderBy(desc('number_of_tours'))\
                .filter('number_of_tours>5000')
    return serps.select(col('serp_url')).rdd.map(lambda x: x[0]).collect()


def get_d_serp_data(serp_input: str):
    """filter d serps

    Args:
        serp_input (str)
    Returns:
        SparkDataFrame
    """
    serps = read_spark_csv(input=serp_input)
    serps = serps.filter(serps['serp_type'] == 'd')\
                 .select(col('tour_id').alias('tour_id1').cast(IntegerType()),
                         col('serp_url'))
    return serps
