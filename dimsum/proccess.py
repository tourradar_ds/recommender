from preferences import get_action_importances, aggregate_user_preferences
from read_write import read_spark_csv
from pyspark.sql.functions import dense_rank
from pyspark.sql.types import IntegerType
from pyspark.sql.window import Window


def preprocess(sc_files: str):
    """aggregate user preferences

    Args:
        data(SparkDataFrame): SparkDataFrame
        column (str): column name to aggregate
        action_importances(list): list of preferences

    Returns:
        list
    """
    data = read_spark_csv(input=sc_files)
    importances = get_action_importances(click_value=0.5,
                                         book_now_value=4,
                                         purchase_value=10)
    data_agg = aggregate_user_preferences(data=data,
                                          column='type',
                                          action_importances=importances)
    return data_agg


def StringToInt(dataframe):
    """Change string to int

    Args:
        dataframe: SparkDataframe with columns (user_id, tour_id, type)

    Returns:
        SparkDataFrame: transformed dataframe

    """
    transform_data = dataframe.withColumn('user_id_int', (dense_rank().over(Window().orderBy("user_id"))).cast(IntegerType()))\
                              .withColumn("tour_id_int", dataframe['tour_id'].cast(IntegerType()))\
                              .withColumn("ratings", dataframe['type'].cast(IntegerType()))
    return transform_data
