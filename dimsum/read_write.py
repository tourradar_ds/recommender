import os
from pyspark.sql import SparkSession, SQLContext
from pyspark.sql.types import IntegerType, DoubleType, StructType, StructField
spark = SparkSession.builder.appName('Recommender').getOrCreate()
sc = spark.sparkContext
sqlContext = SQLContext(sc)


def read_spark_csv(input: str):
    """read csv file/files in spark

    Args:
        input: path to csv file/files

    Returns:
        SparkDataFrame

    """
    df = spark.read.format("csv")\
              .option("header", "true")\
              .load(input)
    return df


def write_spark_csv(data, output_name: str):
    """write csv file/files in spark

    Args:
        data: SparkDataFrame
        output_name: path to save csv file/files

    """
    data.write.format("csv")\
              .mode('overwrite')\
              .option("header", "true")\
              .save(output_name)


def write_spark_csv_one_file(data, output_name: str):
    """write csv file/files in spark

    Args:
        data: SparkDataFrame
        output_name: path to save csv file/files

    """
    data.repartition(1)\
        .write.format("csv")\
        .mode("overwrite")\
        .option("header", "true")\
        .save(output_name)


def make_outpath(outdir: str, subdir: str, filename: str):
    """create outputput

    Args:
        outdir: main directory
        subdir: sub directory
        filename:
    Returns:
        path

    """
    os.makedirs(os.path.join(outdir, subdir), exist_ok=True)
    return os.path.join(outdir, subdir, filename)


def create_empty_dataframe():
    """create empty dataframe

    """
    schema = StructType([
        StructField("tour_id", IntegerType()),
        StructField("similar_tour_id", IntegerType()),
        StructField("similarity", DoubleType())
    ])
    return spark.createDataFrame(sc.emptyRDD(), schema)