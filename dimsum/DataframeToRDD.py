def convert_to_RDD_UI(data):
    """transform user_item dataframe to rdd list UI (user (item,score)

    Args:
        data(SparkDataFrame): SparkDataFrame

    Returns:
        RDD list (x.user_id, (x.tour_id, x.type)))
    """
    data_rdd = data.rdd.map(lambda x: (x.user_id, (x.tour_id, x.type)))
    return data_rdd.groupByKey().mapValues(list)


def convert_to_RDD_IU(data):
    """transform user_item dataframe to rdd list UI (item (user,score)

    Args:
        data(SparkDataFrame): SparkDataFrame

    Returns:
        RDD list (x.user_id, (x.tour_id, x.type)))
    """
    data_rdd = data.rdd.map(lambda x: (x.tour_id, (x.user_id, x.type)))
    return data_rdd.groupByKey().mapValues(list)