import operator
import logging
import random
import math
from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('Recommender').getOrCreate()

sc = spark.sparkContext


logger = logging.getLogger(__name__)


def calculate_norm(data):
    """Calculate norm of the tour values ||c||=√c_1^2+...+c_n^2
    Args:
        data: RDD with values (user_id, [(tour_id, type)...]...)

    Returns:
        Dict: {tour_id: norm}

    """
    def mapper(row):
        return (row[1])
    def mapper1(row):
        yield (row[0], row[1]**2)
    norms = (data.flatMap(mapper)
                 .flatMap(mapper1)
                 .reduceByKey(operator.add)
                 .map(lambda x: (x[0], math.sqrt(x[1])))
                 .collect())
    return {i: j for i, j in norms}


def calculate_gamma(norms, threshold):
    """Calculate gamma according to the paper https://arxiv.org/pdf/1304.1467.pdf
    Important:similarities are preserved if gamma γ= Ω(log(n)/s)
    γ= Ω(log(n)/s)=k*log(n)/s
    (where s is threshold similarity, k is some constant, n is number of tours)
    k is assigned to 4 as advised here https://blog.twitter.com/engineering/en_us/a/2014/all-pairs-similarity-via-dimsum.html
    Args:
        norms: tours norms Dictionary
        threshold: similarity threshold
    Returns:
        int: square root of gamma
    """
    sqrt_gamma = (math.sqrt(4 * math.log(len(norms)) / threshold) if threshold > 1e-6 else float("inf"))
    return sqrt_gamma


def tour_pq(sc, data, threshold):
    """Calculate probability and factor for each tour
    Args:
        data: RDD with values (user_id, [(tour_id, type)...]..)
        threshold: similarities above this value will be preserved
    Returns:
        broadcasted dict: {tour id:(p,q)}
        p = min(1,(gamma / ||c||))
        q = min(gamma,||c||)
        ||c|| is norm of the tour
    """
    norms = calculate_norm(data=data)
    gamma = calculate_gamma(norms=norms, threshold=threshold)
    pq = sc.broadcast({i: (min(1, (gamma / j)), min(gamma, j))
                      for i, j in norms.items()})
    return pq


def _run_DIMSUM(row, pq_b):
    """Implements DIMSUM as describe here:
    http://arxiv.org/abs/1304.1467
    Args:
        row: list with values (user_id, [(tour_id, type)...])
    Returns:
        similarities between tour_ids in the form
              [(tour_id, tour_id, similarity)]
    """
    for i in range(len(row)):
        if random.random() < pq_b.value[row[i][0]][0]:
            for j in range(i + 1, len(row)):
                if random.random() < pq_b.value[row[j][0]][0]:
                    value_i = row[i][1] / pq_b.value[row[i][0]][1]
                    value_j = row[j][1] / pq_b.value[row[j][0]][1]
                    key = ((row[i][0], row[j][0]) if row[i][0] < row[j][0]
                           else (row[j][0], row[i][0]))
                    yield (key, value_i * value_j)


def get_similarity_data(data):
    """Turns similarities into the final neighborhood matrix. The schema
    for saving the matrix is like {tour_id: [(tour_id, similarity1)...]}
    :type neighbor_uri: str
    :param neighbor_uri: uri for where to save the matrix.
    :type data: RDD
    :param data: RDD with data like [((tour_id, tour_id), similarity)]
    """
    def duplicate_keys(row):
        """Builds the similarities between both the diagonals
        of the similarity matrix. In the DIMSUM algorithm, we just compute
        one of the diagonals. Here we will add the transpose of the matrix
        so Marreco can see all similarities between all skus.
        :type row: list
        :param row: data of type [(tour_id, tour_id), similarity]
        :rtype: list:
        :returns: skus and their transposed similarities, such as
                  [tour_id, [tour_id, s]], [tour_id, [tour_id, s]]
        """
        yield (row[0][0], [(row[0][1], row[1])])
        yield (row[0][1], [(row[0][0], row[1])])
    df = (data.flatMap(lambda x: duplicate_keys(x))
          .reduceByKey(operator.add)
          .toDF(['tour_id', 'similar_tour']))
    return df
