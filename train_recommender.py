import pandas as pd
import numpy as np
import logging
from datetime import datetime
from model import get_model
from read_write import make_outpath
from args import make_parser_item_item
from log import setup_logger
from train_graph_embeddings import train_graph_embeddings
from confidence import adjust_ratios
from pareto import pareto_boost
from boost import get_tour_boost
from tour_score import prepare_tour_score
from similarity import prepare_dimsum_similarity, combine_similarities
from dslogger import init_logger


logger = init_logger({
    "app_env": "development",
    "is_development": True
    }, log_level=10, log_dataframe_stats=True)


def run_recommender(dimsum_similarity_input: str,
                    conversion_input: str,
                    clicks_impressions_input: str,
                    graph_embeddings_input: str,
                    weight_threshold: int,
                    n_users: int,
                    k_similar_tours: int,
                    tour_score_input: str,
                    output_path: str
                    ):
    """RUN recommender.
    Args:
        dimsum_similarity_input (str):
        conversion_input (str):
        clicks_impressions_input (str):
        graph_embeddings_input (str):
        weight_threshold (int):
        n_users (int):
        k_similar_tours (int):
        tour_availability_input (str):
        tour_score_input (str):
        output_path (str):
    """
    # -- Load supporting input dataframes
    rev = pd.read_csv(conversion_input, dtype={"tour_id": np.int64})
    logger.debug("Dataframe debug",
                 extra={"dataframe": {"type": "info",
                                      "df": rev,
                                      "name": "assisted_conversion"}})
    ctr = pd.read_csv(clicks_impressions_input, dtype={"tour_id": np.int64})
    logger.debug("Dataframe debug",
                 extra={"dataframe": {"type": "info",
                                      "df": ctr,
                                      "name": "clicks_impressions"}})
    tour_score = pd.read_csv(tour_score_input, dtype={"tour_id": np.int64})
    logger.debug("Dataframe debug",
                 extra={"dataframe": {"type": "info",
                                      "df": tour_score,
                                      "name": "clicks_impressions"}})
    dimsum_similarity = pd.read_csv(dimsum_similarity_input,
                                    dtype={"tour_id": np.int64})

    # -- Clean ctr and ac data
    ctr_rev_data = adjust_ratios(ctr_df=ctr, revenue_df=rev)
    logger.debug("Dataframe debug",
                 extra={"dataframe": {"type": "info",
                                      "df": ctr_rev_data,
                                      "name": "ratios_corrected"}})
    pareto_boost_data = pareto_boost(data=rev, days=400)
    logger.debug("Dataframe debug",
                 extra={"dataframe": {"type": "info",
                                      "df": pareto_boost_data,
                                      "name": "pareto_boost_clean"}})
    tour_score_data = prepare_tour_score(tour_score_df=tour_score)
    logger.debug("Dataframe debug",
                 extra={"dataframe": {"type": "info",
                                      "df": tour_score_data,
                                      "name": "tour_score_corrected"}})
    # combine different boosts
    boost_data = get_tour_boost(pareto_boost_df=pareto_boost_data,
                                ctr_df=ctr_rev_data,
                                tour_score_df=tour_score_data)
    logger.debug("Dataframe debug",
                 extra={"dataframe": {"type": "info",
                                      "df": boost_data,
                                      "name": "combine boosts"}})

    # -- Run Dimsum
    similarity_data_scaled_pd = prepare_dimsum_similarity(dimsum_similarity_df=dimsum_similarity)
    logger.debug("Dataframe debug",
                 extra={"dataframe": {"type": "info",
                                      "df": similarity_data_scaled_pd,
                                      "name": "dimsum_similarity_data"}})
    # -- Run Graph Embeddings
    enhanced_graph_embeddings = train_graph_embeddings(
                                                interactions_input=graph_embeddings_input,
                                                weight_threshold=weight_threshold,
                                                n_users=n_users,
                                                k_similar_tours=k_similar_tours
                                                )
    logger.debug("Dataframe debug",
                 extra={"dataframe": {"type": "info",
                                      "df": enhanced_graph_embeddings,
                                      "name": "graph_similarity_data"}})
    # -- Combine similarities
    graph_dimsum_sim = combine_similarities(
                        graph_similarity_df=enhanced_graph_embeddings,
                        dimsum_similarity_df=similarity_data_scaled_pd)

    logger.debug("Dataframe debug",
                 extra={"dataframe": {"type": "info",
                                      "df": graph_dimsum_sim,
                                      "name": "combine graph and dimsum similarities"}})
    # -- Get final model
    model = get_model(boost_df=boost_data, similarity_df=graph_dimsum_sim)
    logger.debug("Dataframe debug",
                 extra={"dataframe": {"type": "info",
                                      "df": model,
                                      "name": "final model"}})

    # Define Output paths
    raw_path = make_outpath(output_path, "dimsum/raw", f"similarity_{datetime.now().date()}.csv")
    raw_path_graph = make_outpath(output_path, "dimsum/raw", f"similarity_graph{datetime.now().date()}.csv")
    raw_path_revenue = make_outpath(output_path, "dimsum/raw", f"rev_{datetime.now().date()}.csv")
    raw_path_ctr = make_outpath(output_path, "dimsum/raw", f"ctr_{datetime.now().date()}.csv")
    model_path = make_outpath(output_path, "dimsum/model", f"model_{datetime.now().date()}.csv")
    # save files
    similarity_data_scaled_pd.to_csv(raw_path, index=False)
    enhanced_graph_embeddings.to_csv(raw_path_graph, index=False)
    pareto_boost_data.to_csv(raw_path_revenue, index=False)
    ctr_rev_data.to_csv(raw_path_ctr, index=False)
    model.to_csv(model_path, index=False)
    # rank_recommender.to_csv(output_path, index=False)


def main():
    """Make cli, run trainer.
    """
    setup_logger()

    parsed = make_parser_item_item()
    logger = logging.getLogger(__name__)
    logger.info("Options: %s", parsed)
    run_recommender(
        dimsum_similarity_input=parsed.input_dimsum,
        conversion_input=parsed.revenue_bookings,
        clicks_impressions_input=parsed.clicks_impressions,
        graph_embeddings_input=parsed.input_graph,
        weight_threshold=parsed.weight_tr,
        n_users=parsed.n_users,
        k_similar_tours=parsed.k_nn,
        tour_score_input=parsed.tour_score,
        output_path=parsed.output,
    )


if __name__ == "__main__":
    main()