import argparse

__VERSION__ = "1.0.0"


def make_parser_item_item():
    """Make command line parser."""
    parser = argparse.ArgumentParser(
        description=f"%(prog)s (version {__VERSION__}). RECOMMENDER.",
        epilog="~~~ TourRadar [DS] ~~~",
    )
    parser.add_argument(
        "--version", action="version", version=f"%(prog)s (version {__VERSION__})"
    )
    parser.add_argument(
        "-i_graph",
        "--input_graph",
        help=f"item user interaction for graph embedings",
        metavar="item_user_interaction_graph",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-i_dimsum",
        "--input_dimsum",
        help=f"dimsum_similarity_input",
        metavar="dimsum_similarity_input",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-ctr",
        "--clicks_impressions",
        help=f"Input clicks_impressions file",
        metavar="CLICKSIMPRESSIONS",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-rev_bookings",
        "--revenue_bookings",
        help=f"conversions file",
        metavar="CONVERSIONS",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-tour_param",
        "--tour_parameters",
        help=f"Input tour_parameters file",
        metavar="TOURPARAMETERS",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-tr_score",
        "--tour_score",
        help=f"Input tour_score file",
        metavar="TOURSCORE",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-o",
        "--output",
        help=f"Output path",
        metavar="OUTPUT",
        type=str,
        required=True
    )
    parser.add_argument(
        "--weight_tr",
        help="weight threshold",
        type=int,
        default=1.0
    )
    parser.add_argument(
        "--k_nn",
        help="k similar tours",
        type=int,
        default=700
    )
    parser.add_argument(
        "--n_users",
        help="number of users to sample",
        type=int,
        default=-1
    )
    return parser.parse_args()
