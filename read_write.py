import os


def make_outpath(outdir: str, subdir: str, filename: str):
    """create outputput

    Args:
        outdir: main directory
        subdir: sub directory
        filename:
    Returns:
        path

    """
    os.makedirs(os.path.join(outdir, subdir), exist_ok=True)
    return os.path.join(outdir, subdir, filename)
