import pandas as pd


def get_model(boost_df: pd.DataFrame, similarity_df: pd.DataFrame):
    """ combine similarity and the boost

    Args:
        boost_df (pd.DataFrame):
        similarity_df (pd.DataFrame):

    Returns:
        pd.DataFrame: model

    """
    model = similarity_df.merge(boost_df,
                                left_on='similar_tour_id',
                                right_on='tour_id',
                                how='left').fillna(1)
    model['boost'] = model['similarity'] * model['boost_scaled']
    return model[['tour_id_x', 'similar_tour_id', 'boost']].rename(columns={'tour_id_x': 'tour_id'})
