import pandas as pd
import numpy as np


def process_interactions(interactions_df: pd.DataFrame,
                         weight_threshold: int = -1,
                         n_users: int = -1) -> pd.DataFrame:
    """prepare interactions for graph

    Args:
        tour_parameters_df (pd.DataFrame):
            id, duration_days, price_eur, tour_reviews, tour_rating
        interactions_df (pd.DataFrame):

    Returns:
        pd.DataFrame: source,target, weight

    """
    # Use clever sorting instead of groupby
    df = interactions_df.rename(
        columns={"tour_id": "source",
                 "ga_sessions_id": "user_id"}).sort_values(by=['user_id', 'rank'])

    # Set source from target or -1 if users do not match
    df["target"] = np.where(
        df["user_id"] == np.roll(df["user_id"].values, -1),
        np.roll(df["source"].values, -1), -1)

    # Filter out rows where users didn't match (target == -1)
    df = df[df["target"] >= 0]
    df["weighted"] = np.where(df['have_booking'] == 1, 10, 1)

    # Create weight (add 1 per user seing edge, 10 for sessions with bookings)
    df["weight"] = df.groupby(
        ['source', 'target'],
        observed=True)["weighted"].transform('sum')
    df = df[['user_id', 'source', 'target', 'weight']]

    # Filter out weakest links
    if weight_threshold > 0:
        df = df[df["weight"] > weight_threshold]

    # Sample subset of all users
    if n_users > 0 and n_users <= df['user_id'].nunique():
        df = df[
            df['user_id'].isin(
                pd.Series(
                    df['user_id'].unique())
                .sample(n_users))]

    # Return relevant cols without duplicates
    return df[["source", "target", "weight"]].drop_duplicates()
