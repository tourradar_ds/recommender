import pandas as pd
import networkx as nx
from node2vec import Node2Vec
from edges import process_interactions


def build_graph(Edge_df: pd.DataFrame) -> nx.DiGraph:
    """Build directed graph from weighted edges dataframe

    Args:
        Edge_df(pd.DataFrame): source, target, weight
    Returns:
        Graph

    """
    G = nx.DiGraph()
    G.add_weighted_edges_from(
        zip(Edge_df["source"], Edge_df["target"], Edge_df["weight"]))
    G.remove_nodes_from(nx.isolates(G))
    return G


def build_node2vec(graph):
    """Build node2vec model

    Args:
        graph(nx.Graph):
    Returns:
        node2vect model

    """
    n2v_obj = Node2Vec(graph,
                       dimensions=50,
                       walk_length=15,
                       num_walks=30,
                       p=1,
                       q=1,
                       workers=3)
    n2v_model = n2v_obj.fit()
    return n2v_model


def get_similarities(
        n2v_model,
        G,
        k_similar_tours: int) -> pd.DataFrame:
    """Build node2vec model
    Args:
        n2v_model:
        edge_df (pd.DataFrame)
        k_similar_tours (int)
    Returns:
        pd.DataFrame
    """
    tour_ids = set(list(G.nodes()))
    temp = []
    tours = []
    for i in tour_ids:
        result = n2v_model.wv.most_similar(str(i), topn=k_similar_tours)
        temp.extend(result)
        tours.extend([str(i)]*len(result))
    res = pd.DataFrame(temp, columns=['similar_tour_id', 'graph_similarity'])
    res['tour_id'] = tours
    return res[['tour_id', 'similar_tour_id', 'graph_similarity']]


def get_tour_tour_similarity(data: pd.DataFrame) -> pd.DataFrame:
    """remove categories from similarities

    Args:
        similarity_df (pd.DataFrame): output
    Returns:
        pd.DataFrame

    """
    tours = list(data['tour_id'].unique())
    tour_sim = data[data['similar_tour_id'].isin(tours)]
    tour_sim['tour_id'] = tour_sim['tour_id'].astype(int)
    tour_sim['similar_tour_id'] = tour_sim['similar_tour_id'].astype(int)
    return tour_sim


def train_graph_embeddings(
        interactions_input: str,
        weight_threshold: int,
        n_users: int,
        k_similar_tours: int) -> pd.DataFrame:
    """Train Graph Model

    Args:
        tour_parameters_df (pd.DataFrame):
        interactions_df (pd.DataFrame):
        weight_threshold (int):
        n_users (int):
        k_similar_tours: str
    Returns:
        pd.DataFrame

    """
    interactions_df = pd.read_csv(interactions_input, low_memory=False)

    interactions_df = process_interactions(
        interactions_df=interactions_df,
        weight_threshold=weight_threshold,
        n_users=n_users)
    df_edges = interactions_df
    G = build_graph(Edge_df=df_edges)
    node2vec_model = build_node2vec(graph=G)
    similarity_df = get_similarities(
        n2v_model=node2vec_model,
        G=G,
        k_similar_tours=k_similar_tours)
    tour_tour_sim = get_tour_tour_similarity(data=similarity_df)
    return tour_tour_sim
