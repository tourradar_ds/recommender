import pandas as pd
import numpy as np
from datetime import datetime, timedelta


def pareto_revenue(data: pd.DataFrame, days: int) -> pd.DataFrame:
    """summarize revenue data

    Args:
        data (pd.DataFrame): assisted conversion dataframe from omnimove

    Returns:
        pd.DataFrame: summarized assisted conversion dataframe

    """
    treshold = pd.Timestamp(datetime.now().date() - timedelta(days=days))
    ac_stats = data[
        pd.to_datetime(data['booking_date']) > treshold]\
        .groupby(["tour_id"], observed=True, sort=False, as_index=False)\
        .agg({'net_revenue': 'sum'})
    return ac_stats


def pareto_boost(data: pd.DataFrame, days: int) -> pd.DataFrame:
    """summarize assisted conversions file. boost tours that make 80 % of revenue

    Args:
        df (pd.DataFrame): summarized assisted conversion

    Returns:
        pd.DataFrame: dataframe with pareto boost

    """
    df_revenue = pareto_revenue(data=data, days=days)
    df_sorted = df_revenue.sort_values(by=['net_revenue'], ascending=False)
    total_fr_per = 100 * df_sorted['net_revenue'].\
        transform(lambda x: x.cumsum() / x.sum())
    df_sorted['pareto_boost'] = np.where(total_fr_per <= 80, 1.2, 1)
    return df_sorted[['tour_id', 'pareto_boost']]
