import logging
import sys


def setup_logger():
    level = logging.INFO
    c_format = logging.Formatter(
        "%(asctime)s - %(levelname)s - %(message)s")
    c_handler = logging.StreamHandler(sys.stdout)
    c_handler.setLevel(level)
    c_handler.setFormatter(c_format)
    logging.getLogger('').addHandler(c_handler)
    logging.getLogger('').setLevel(level)


logger = logging.getLogger(__name__)