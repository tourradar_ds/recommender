import pandas as pd
from sklearn.preprocessing import minmax_scale


def scale_similarity(df: pd.DataFrame) -> pd.DataFrame:
    """get new boost value.

    Args:
        df (pd.DataFrame): pair wise similarity

    Returns:
        pd.DataFrame: dataframe with new scaled value

    """
    df['dimsum_similarity'] = df.groupby('tour_id')['similarity'].\
        transform(lambda x: minmax_scale(x.astype(float), feature_range=(1, 2)))
    return df
