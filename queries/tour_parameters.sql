SET group_concat_max_len = 1000000;

-- temporary table for destinations --
CREATE TEMPORARY TABLE gr_tmp_tours_destinations (
    tour_id int,
    cities text,
    countries text,
    regions text,
    continents text,
    PRIMARY KEY (tour_id)
)
SELECT
    _tl.tour_id,
    group_concat(distinct _c.id order by _tl.id separator '|') AS cities,
    group_concat(distinct _cn.id order by _tl.id separator '|') AS countries,
    group_concat(distinct _rc.region_id order by _tl.id separator '|') AS regions,
    group_concat(distinct _cn.continent_id order by _cn.continent_id separator '|') AS continents
FROM
    tours_locations _tl
    JOIN cities _c ON _tl.city_id = _c.id
    JOIN countries _cn ON _tl.country_id = _cn.id
    LEFT JOIN regions_countries _rc ON _cn.id = _rc.country_id
GROUP BY
    _tl.tour_id;

-- temporary table for travel styles --
CREATE TEMPORARY TABLE gr_tmp_tours_travel_styles (
    tour_id int,
    travel_style text,
    secondary_tour_types text,
    PRIMARY KEY (tour_id)
)
SELECT
    _t.id AS tour_id,
    _tth.id AS travel_style,
    group_concat(distinct if(_ttt.type_id != _t.type, _ttt.type_id, null) order by _ttt.type_id separator '|') AS secondary_tour_types
FROM
    tours _t
    LEFT JOIN tour_themes_types _ttty ON _t.type = _ttty.type_id
    LEFT JOIN tour_themes _tth ON _ttty.theme_id = _tth.id
    LEFT JOIN tours_tour_types _ttt ON _t.id = _ttt.tour_id
GROUP BY
    _t.id;


-- temporary table for departures --
CREATE TEMPORARY TABLE gr_tmp_tours_departures (
    tour_id int,
    departures text,
    PRIMARY KEY (tour_id)
)
SELECT
    tour_id,
    group_concat(distinct date_format(date, '%Y-%m') ORDER BY date separator '|') AS departures
FROM
    gr_inventory_departures
WHERE
    date >= curdate()
    AND date <= last_day(date_add(curdate(), interval 17 month))
GROUP BY
    tour_id;

-- ETL_QUERY_BEGIN
SELECT
    _t.id,
    _o.id AS operator_id,
    if(_t.length_type = 'd', _t.length, 1) AS duration_days,
    _tpc.eur AS price_eur,
    coalesce(_tv.reviews, 0) AS tour_reviews,
    if(_tv.reviews_rating > 0, _tv.reviews_rating, null) AS tour_rating,
    if(_o.payment_model in (6, 7) and (_o.tourcms_bookings = '1' or _t.auto_confirm = 'yes' or _o.instant_booking = '1'), 1, 0) AS tour_instant,
    _ti.departures AS departure_months,
    _td.cities AS cities,
    _cs.id AS city_start,
    _ce.id AS city_end,
    _tts.travel_style,
    group_concat(distinct _at.id separator '|') AS accommodations,
    group_concat(distinct _tt.id separator '|') AS transport,
    _t.type AS main_tour_type,
    _tts.secondary_tour_types,
    _td.continents,
    _td.countries,
    _td.regions,
    if(_t.min_age > 0 or _t.max_age > 0, if(_t.min_age > 0, _t.min_age, null), if(_o.age_group_min > 0 or _o.age_group_max > 0, if(_o.age_group_min > 0, _o.age_group_min, null), null)) AS age_range_min,
    if(_t.min_age > 0 or _t.max_age > 0, if(_t.max_age > 0, _t.max_age, null), if(_o.age_group_min > 0 or _o.age_group_max > 0, if(_o.age_group_max > 0, _o.age_group_max, null), null)) AS age_range_max
FROM
    tours _t
    JOIN operators _o ON _t.operator_id = _o.id
    JOIN users _u ON _o.user_id = _u.id
    JOIN tours_activity _ta ON _t.id = _ta.tour_id
    JOIN tours_prices_calculated _tpc ON _t.id = _tpc.tour_id
    LEFT JOIN tours_values _tv ON _t.id = _tv.tour_id
    LEFT JOIN gr_tmp_tours_departures _ti ON _t.id = _ti.tour_id
    LEFT JOIN gr_tmp_tours_destinations _td ON _t.id = _td.tour_id
    LEFT JOIN tours_accommodation_types _tat ON _t.id = _tat.tour_id
    LEFT JOIN accommodation_types _at ON _tat.type_id = _at.id
    LEFT JOIN tours_transport_types _ttt ON _t.id = _ttt.tour_id
    LEFT JOIN transport_types _tt ON _ttt.type_id = _tt.id
    LEFT JOIN gr_tmp_tours_travel_styles _tts ON _t.id = _tts.tour_id
    LEFT JOIN cities _cs ON _t.start_city_id = _cs.id
    LEFT JOIN cities _ce ON _t.end_city_id = _ce.id
WHERE
    _ta.active = '1'
    AND _tpc.eur > 0
GROUP BY
    _t.id;

-- ETL_QUERY_END

-- removing temporary tables --
DROP TEMPORARY TABLE gr_tmp_tours_destinations;
DROP TEMPORARY TABLE gr_tmp_tours_departures;
DROP TEMPORARY TABLE gr_tmp_tours_travel_styles;
