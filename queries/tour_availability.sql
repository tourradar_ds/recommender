-- Converts currency values to availability counts per currency
--
-- Logic on when tour is available on SERP
-- https://tourradar.atlassian.net/wiki/spaces/TD/pages/564461631/Tour+is+able+to+be+on+SERP
SELECT
    tv_.tour_id,
    DATE(FROM_UNIXTIME(tv_.start)) AS start_date,
    CASE WHEN tv_.hide_if_zero = 1 AND tv_.aud = 0 THEN 0 ELSE tv_.availability END AS availability_aud,
    CASE WHEN tv_.hide_if_zero = 1 AND tv_.cad = 0 THEN 0 ELSE tv_.availability END AS availability_cad,
    CASE WHEN tv_.hide_if_zero = 1 AND tv_.eur = 0 THEN 0 ELSE tv_.availability END AS availability_eur,
    CASE WHEN tv_.hide_if_zero = 1 AND tv_.gbp = 0 THEN 0 ELSE tv_.availability END AS availability_gbp,
    CASE WHEN tv_.hide_if_zero = 1 AND tv_.nzd = 0 THEN 0 ELSE tv_.availability END AS availability_nzd,
    CASE WHEN tv_.hide_if_zero = 1 AND tv_.usd = 0 THEN 0 ELSE tv_.availability END AS availability_usd,
    CASE WHEN tv_.hide_if_zero = 1 AND tv_.inr = 0 THEN 0 ELSE tv_.availability END AS availability_inr
FROM
    tours_variants tv_
JOIN tours_activity ta_ ON ta_.tour_id = tv_.tour_id
JOIN tours_prices_calculated tpc_ ON ta_.tour_id = tpc_.tour_id
WHERE
    tv_.availability > 0
    AND ta_.active = '1'
    AND tpc_.eur > 0
    AND (tv_.departure_type NOT IN ('cancelled', 'closed') OR tv_.departure_type IS NULL)
HAVING
    start_date BETWEEN DATE_ADD(CURDATE(), INTERVAL 3 DAY) AND DATE_ADD(CURDATE(), INTERVAL 2 YEAR)
