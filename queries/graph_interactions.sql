with interactions as (
  SELECT
        CONCAT(CAST(ga_sessions.fullVisitorId AS STRING), '|', COALESCE(CAST(ga_sessions.visitStarttime AS STRING),''),'|',COALESCE(CAST(ga_sessions.visitnumber AS STRING),''))  AS ga_sessions_id,
     fullvisitorid as user_id,
     hits_product.productSku as tour_id,
     CONCAT(ga_sessions.date,"-",hits.hour,"-",hits.minute) as date,
     CASE WHEN hits.ecommerceAction.action_type = '2' THEN 1
        WHEN hits.ecommerceAction.action_type = '5' and ecommerceAction.step = 1 THEN 2
        WHEN  hits.ecommerceAction.action_type = '6' then 3
     END type ,
     count(*) as n
  FROM `bigqueryanalytics360.33278945.ga_sessions*` AS ga_sessions
      JOIN UNNEST(ARRAY( (SELECT AS STRUCT * FROM UNNEST(ga_sessions.hits)))) as hits
      JOIN UNNEST(hits.product) as hits_product
  WHERE TRUE
      and ((ecommerceAction.action_type in ('2', '6'))  or (hits.ecommerceAction.action_type = '5' and ecommerceAction.step = 1))
        and CAST(PARSE_TIMESTAMP('%Y%m%d', ga_sessions.date)  AS DATE) > DATE_ADD(CURRENT_DATE(), INTERVAL -120 DAY)
  GROUP BY 1,2,3,4,5
),
bookers as 
(
SELECT
  CONCAT(CAST(ga_sessions.fullVisitorId AS STRING), '|', COALESCE(CAST(ga_sessions.visitStarttime AS STRING),''),'|',COALESCE(CAST(ga_sessions.visitnumber AS STRING),''))  AS ga_sessions_id,
  1 as have_booking
FROM `bigqueryanalytics360.33278945.ga_sessions_*`  AS ga_sessions
WHERE (case when
         (select max(totals.transactions) from unnest([ga_sessions.totals]) as totals) >= 1
         then true else false end)
),
interactions_summary as
(
  SELECT
        ga_sessions_id,
        count(distinct tour_id) as n
  FROM interactions
  GROUP BY 1
  ORDER BY 2 DESC
),
quantile as
(
  SELECT distinct
          ga_sessions_id,
          n,
          PERCENTILE_CONT(n, 0.999) OVER() AS quantile_99_per
    FROM interactions_summary
),
sessions_for_analysis as
(
  SELECT
    ga_sessions_id,
    tour_id,
    date,
    type
  FROM interactions
  WHERE ga_sessions_id in
                          (select distinct ga_sessions_id
                            from quantile
                            where n>1 and n<=quantile_99_per)
)



SELECT
  ga_sessions_id,
  tour_id,
  min_order,
  ROW_NUMBER() OVER (PARTITION BY ga_sessions_id ORDER BY min_order) AS rank,
  coalesce(have_booking,0) as have_booking
FROM
(
  SELECT ga_sessions_id,
         tour_id,
         min(dense_rank1) as min_order
  FROM
(
  SELECT *,
        ROW_NUMBER() OVER (PARTITION BY ga_sessions_id ORDER BY date) AS dense_rank1
  FROM sessions_for_analysis)
  GROUP BY 1,2
)
left join bookers using(ga_sessions_id)