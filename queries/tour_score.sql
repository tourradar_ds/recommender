-- from redshift

select
   tour_id,
   (
       p_additional_info
        + p_departure_dates
        + p_itinerary_details
        + p_photos
        + p_reviews_total
        + p_reviews_verified_rating
        + p_reviews_response_rate
        + p_reviews_rating
        + p_instant_bookable
        + p_enquiries_response_time
        + p_enquiries_cancellation_rate) as tour_score
from analytics.bdm_tour_score  ts
where is_active = true
