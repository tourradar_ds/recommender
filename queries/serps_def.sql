# Statistics DB

SELECT
    _sp.id AS serp_id,
    _sp.type AS serp_type,
    _sp.params AS serp_name,
    _tch.tour_id AS tour_id,
    CONCAT('/', _sp.type, '/',  _sp.params) AS serp_url
FROM
    tours_cache_high _tch
    JOIN serp_pages _sp ON _sp.id = _tch.page_id
    JOIN tours_activity _ta ON _ta.tour_id = _tch.tour_id
WHERE
    _ta.active = '1'
    AND _sp.active_tours > 3
GROUP BY
    1,2,3,4
