with clicks_impressions as
(
SELECT
ga_sessions.fullVisitorId,
CAST(PARSE_TIMESTAMP('%Y%m%d', ga_sessions.date)  AS DATE) AS date,
hits_product.isImpression,
hits_product.isClick,
hits_product.productSKU as tour_id
FROM `bigqueryanalytics360.33278945.ga_sessions_*`  AS ga_sessions
    LEFT JOIN UNNEST([ga_sessions.geoNetwork]) as geoNetwork
    LEFT JOIN UNNEST(ARRAY( (SELECT AS STRUCT * FROM UNNEST(ga_sessions.hits)))) as hits
    LEFT JOIN UNNEST([hits.page]) as hits_page
    LEFT JOIN UNNEST(hits.product) as hits_product
GROUP BY 1,2,3,4,5
),
summary_table as
(
select
    tour_id,
    count(distinct CASE WHEN  isImpression is NOT NULL THEN fullVisitorId ELSE NULL END) AS impressions,
    count(distinct CASE WHEN isClick is NOT NULL THEN fullVisitorId ELSE NULL END) AS clicks
from clicks_impressions
where date>='2019-11-01'
group by 1
)

select *,
100*clicks/impressions as ctr
from summary_table
where clicks>10 and impressions>100
order by 4 desc
