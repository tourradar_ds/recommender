with user_item as
(
    SELECT
           fullvisitorid as user_id,
           hits_product.productSku as tour_id,
           CASE WHEN hits.ecommerceAction.action_type = '2' THEN 1
              WHEN hits.ecommerceAction.action_type = '5' and ecommerceAction.step = 1 THEN 2
              WHEN  hits.ecommerceAction.action_type = '6' then 3
           END type ,
           count(*) as n
    FROM `bigqueryanalytics360.33278945.ga_sessions*` AS ga_sessions
            JOIN UNNEST(ARRAY( (SELECT AS STRUCT * FROM UNNEST(ga_sessions.hits)))) as hits
            JOIN UNNEST(hits.product) as hits_product
    WHERE TRUE
          and ((ecommerceAction.action_type in ('2', '6'))  or (hits.ecommerceAction.action_type = '5' and ecommerceAction.step = 1))
          and CAST(PARSE_TIMESTAMP('%Y%m%d', ga_sessions.date)  AS DATE) > DATE_ADD(CURRENT_DATE(), INTERVAL -100 DAY)
    GROUP BY 1,2,3
),
 implicit as
(
    SELECT
        ga_sessions.fullVisitorId as user_id,
        REGEXP_EXTRACT( hits_page.pagePathLevel2, r'/([^(?|#|\-)]+)') AS tour_id,
        case when hits_page.pagePathLevel1 in ('/t/') then 1 when
        hits_page.pagePathLevel1 in ('/book-now/') then 2 end as type,
        count(*) as nbr_actions -- to think
    FROM `bigqueryanalytics360.33278945.ga_sessions*` AS ga_sessions
            JOIN UNNEST(ga_sessions.hits) as hits
            LEFT JOIN UNNEST([hits.page]) as hits_page
    WHERE   CAST(PARSE_TIMESTAMP('%Y%m%d', ga_sessions.date)  AS DATE) > DATE_ADD(CURRENT_DATE(), INTERVAL -100 DAY)
            and hits.type = "EVENT"
            and hits_page.pagePathLevel1 in ('/t/', '/book-now/')
    group by 1,2,3
),
implicit_clean as
(
    SELECT user_id,
        tour_id,
        type,
        case when nbr_actions>quantile_99 then quantile_99 else nbr_actions end as value
    from implicit
         left join (
                    select distinct type,
                    PERCENTILE_CONT(nbr_actions, 0.99) OVER(partition by type) AS quantile_99
                    from implicit
                    ) using(type)
),
implicit_quantile as
(
    SELECT distinct
          type,
          PERCENTILE_CONT(nbr_actions, 0.50) OVER(partition by type) AS quantile_50_value
    FROM implicit
),
minmax_value as
(
    SELECT type,
        min(value) as min,
       ( max(value)-min(value)) as minmax_range
    FROM implicit_clean
    group by 1
 )
, exit as
(
    SELECT
        ga_sessions.fullVisitorId as user_id,
        REGEXP_EXTRACT( hits_page.pagePathLevel2, r'/([^(?|#|\-)]+)') AS tour_id,
        SAFE_CAST(coalesce(max(hits.eventInfo.eventLabel),'0') AS INT64) as perc
    FROM `bigqueryanalytics360.33278945.ga_sessions*` AS ga_sessions
        JOIN UNNEST(ga_sessions.hits) as hits
        LEFT JOIN UNNEST([hits.page]) as hits_page
    WHERE  hits.eventInfo.eventCategory ='Tour Detail' and  hits.eventInfo.eventAction='PageExit - scrollPosition'
        and CAST(PARSE_TIMESTAMP('%Y%m%d', ga_sessions.date)  AS DATE) > DATE_ADD(CURRENT_DATE(), INTERVAL -100 DAY)
        and hits.type = "EVENT"
        and hits_page.pagePathLevel1 = '/t/'
    group by 1,2
),
exit_quantile as
(
    SELECT distinct
          tour_id,
          PERCENTILE_CONT(perc, 0.50) OVER(partition by tour_id) AS quantile_50_per
    FROM exit
)

SELECT user_id,
        tour_id,
        type,
        value*perc as interest
FROM
(
    SELECT user_id,
        tour_id,
        type,
        n,
        coalesce(
        ((value_clean-min)/minmax_range)+1,
        1) as value,
        perc_clean/100 as perc
    FROM
    (
        SELECT user_id,
                tour_id,
                type,
                n,
                coalesce(case when type=3 then 100 else value end ,quantile_50_value) as value_clean,
                coalesce(case when type=3 then 100 else perc end ,quantile_50_per) as perc_clean,
            quantile_50_value,
            quantile_50_per,
            min,
            minmax_range
        FROM user_item
        LEFT JOIN implicit_clean  using(user_id,tour_id,type)
        LEFT JOIN exit using(user_id,tour_id)
        LEFT JOIN implicit_quantile using(type)
        LEFT JOIN exit_quantile using(tour_id)
        LEFT JOIN minmax_value using(type)
    )
    WHERE type in (1,2,3)
)
WHERE n>0 and value>0 and perc>0
